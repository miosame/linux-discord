```
██╗     ██╗███╗   ██╗██╗   ██╗██╗  ██╗       ██╗       ████████╗███████╗ ██████╗██╗  ██╗███╗   ██╗ ██████╗ ██╗      ██████╗  ██████╗██╗   ██╗
██║     ██║████╗  ██║██║   ██║╚██╗██╔╝       ██║       ╚══██╔══╝██╔════╝██╔════╝██║  ██║████╗  ██║██╔═══██╗██║     ██╔═══██╗██╔════╝╚██╗ ██╔╝
██║     ██║██╔██╗ ██║██║   ██║ ╚███╔╝     ████████╗       ██║   █████╗  ██║     ███████║██╔██╗ ██║██║   ██║██║     ██║   ██║██║  ███╗╚████╔╝ 
██║     ██║██║╚██╗██║██║   ██║ ██╔██╗     ██╔═██╔═╝       ██║   ██╔══╝  ██║     ██╔══██║██║╚██╗██║██║   ██║██║     ██║   ██║██║   ██║ ╚██╔╝  
███████╗██║██║ ╚████║╚██████╔╝██╔╝ ██╗    ██████║         ██║   ███████╗╚██████╗██║  ██║██║ ╚████║╚██████╔╝███████╗╚██████╔╝╚██████╔╝  ██║   
╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝    ╚═════╝         ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝ ╚═════╝  ╚═════╝   ╚═╝  
```

# Linux Discord Wiki

> The official and only wiki for [Linux and Technology](https://linuxdiscord.com)

> Favicon made by [dmitri13](https://www.flaticon.com/free-icon/conversation_813020?term=text&page=1&position=31)

# Issues? Contributions?

You are free to [submit PRs or create an issue](https://gitlab.com/miosame/linux-discord/-/issues), if you e.g. find typos, want to suggest sections, ...

# Server FAQ

## What bot do you use?

All our main bots used to be selfhosted instances of [red-discordbot](https://github.com/Cog-Creators/Red-DiscordBot), but now it's a from scratch closed-source bot developed by an administrator of the server, if you're looking for a bot to selfhost though - redbot is still a great way to achieve that, alternatively check out [dyno](https://dyno.gg/), [mee6](https://mee6.xyz/), [tatsu](https://tatsu.gg/) and others.

## Partnership?

We currently do not seek any partnerships, but the conditions below for history sake are still valid.

Basic conditions for us to consider a partnership, which can and will be adjusted in the future:
1. over 50 users (bot and dead/new accounts don't count)
2. does not break any local or international laws (promote pedophilia [drawn or not], blackhat/grayhat hacking, carding, ..)
3. has either some remote connection to technology or the shared user base would benefit from our server and or yours
4. a minimum creation date that dates back 5 months

3 and 4 are not set in stone and can be discussed, just note that we generally don't "seek out" partnerships or need the hassle that comes with it _usually_, we are content with the user base and the increase we get daily.

## Why not manjaro?

TL;DR: unstable, not worth your time, will break a lot.

Why manjaro is "broken by the developers" and why we don't support / help with it? it has many reasons, but the main one being that the maintainers and the developers of the project try their hardest to break the base (arch) that worked due to it being all the time updated - crippling it with outdated, not on time updated dependencies and packages they maintain themselves, causing the system to be unstable, unpredictable and quite frankly none of our business because of it.

## Why is arch not banned then?

Because arch unlike manjaro is the actual "raw" rolling release, not holding packages artificially back and maintaining their own versions of it like manjaro. Use of it generally also implies the user is aware of the issues and wasn't lullabied into believing at any point to expect a long term stable system, instead significant effort and time investment, including keeping up-to-date with announcements to prevent their installation from breaking and even if - being able to [recover](https://wiki.archlinux.org/index.php/General_troubleshooting) from a broken state.

Also if you are absolutely set on using arch - look into BTRFS snapshots at boot, so you can easily come back to a snapshot, if things break.

A great tool is maintained by linux mint called [timeshift](https://github.com/linuxmint/timeshift)

![timeshift](https://raw.githubusercontent.com/linuxmint/timeshift/master/images/settings_schedule.png)

## Why are you not allowing kali/parrot/... linux?

TL;DR: [script-kiddies](https://en.wikipedia.org/wiki/Script_kiddie)  ruin it for everybody and we have zero tolerance for the majority of users of those distros

We have had a significant wave of underage users, with the usual excuses and needs wanting to "hack my friends facebook" and the likes. It's hilarious and ironic to begin with that implied users that need excessive knowledge and a lot of technical understanding to actually do legal "pentesting" need babywheels from our server. But of course it turns out usually it's the [script-kiddie](https://en.wikipedia.org/wiki/Script_kiddie) with malicious intent.

Remember that all your virtual actions [can and will be prosecuted](https://en.wikipedia.org/wiki/Computer_Fraud_and_Abuse_Act) by people that have much less tolerance for your funny business. 

## Any other distros I should avoid?

There's plenty we can't recommend, but haven't outright banned (yet?), some examples include:

- Garuda Linux
    - [distrotube's video snippet](https://wiki.linuxdiscord.com/images/garuda.mp4) pretty much summarizes it amazingly well! - supposed to be just an installer distro on top of arch (like endeavourOS) - that doesn't break anything else, well.. it breaks and it breaks a lot, that's a great achievement (endeavourOS at least installs right once you bash through the older installer)
- Solus
    - pretty much abandoned, core developer drama and everybody significant left the sinking boat long ago
- Regular Debian
    - too outdated, ubuntu has superseded debian by lightyears, there is however more up-to-date versions of debian you could check out, though generally ubuntu is the better choice
- Void
    - just like solus, core developer drama and abandoned
- MX Linux
    - based on debian, so you get all the same old issues, with a handful of mx-tools that make some tasks easier, but can be obtained on any other system too
- Artix and other "no systemd" distros
    - not having systemd, means you'll have a harder time getting most things to work like services or finding any resources)
- Outdated Ubuntus e.g. 16.04
    - repositories from which you'll pull packages stop serving outdated versions and you'll have to tinker to keep it alive, you'll also miss out on any proper security and stability updates 
- elementaryOS
    - it's one of the best looking linux distros out there, but it sadly fails in a lot of support on desktop (mostly fine on laptops) and package updates, it's now 10 years (2021) of elementaryOS and they are still mostly blogging about ideas and the "future" of the project rather than addressing issues, e.g. certain monitor configurations not working right at all or when you were able to force dark mode across all apps just fine and that made the jarring contrast bearable, but then 3 months later they crippled that with the explanation: "we are working on a better one! why would you want to do this??" as with everything else they do, it's chaotic, poorly organized and overall a real nightmare to use, as the devs constantly decide what you should be doing with your hardware and software, not to mention their despicable dark pattern of not making it obvious that you can get their operating system for free by entering $0 in the download menu, which is beyond disgusting considering they are mostly a design effort that has the rest covered by other communities and efforts and even that fails
- centOS
    - abandonware, killed off recently in favor of other spin-offs, fedora should be chosen over centOS if you like rpm
- KDE Neon
    - use Kubuntu 20.04 instead, much more stable KDE and Ubuntu experience, with much more sane defaults out of the box, be aware of [nvidia](beginner-or-quotjust-worksquot-experience-linux-distro) though
- Ubuntu 20.10 / PopOS 20.10 / any .10
    - breaks a lot, unstable, the worst release in decades canonical has pushed out, 21.04 seems stable again though, generally the .04 versions work way better than their .10 counterpart

The general rule of thumb; anything that mentions:

- "from scratch" like void linux / solus
- "rolling release" or "rolling" like arch
- "busybox" like alpine
- "musl" like alpine

avoid them.

# Linux FAQ

## What would be the best distro for virtual machines? (KVM, QEMU, ..)

- Arch if you're an advanced user and are fine with things mentioned [here](#why-is-arch-not-banned-then). Because it has great [documentation](https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF) and support for it.
- Ubuntu - sadly "too many" tutorials available as to making it a pain to find the one that just works, thankfully for 20.04 (latest ubuntu currently) there is a [tutorial](https://www.youtube.com/watch?v=tDMoEvf8Q18) that confirmed works and is very easy to follow. The same guy has other tutorials for previous versions, but just go with latest instead. In case he or youtube takes it down, there's also a [mirror](https://web.archive.org/watch?v=tDMoEvf8Q18), make sure to check the original non mirror first though, as it's exceptionally nice that he added the way for 20.04 right after its release.
- Fedora for things like [cockpit](https://cockpit-project.org/) - though otherwise has very great support for virtualization too.

## I want to try out distro X

### Method 1: virtual machine

You can always just download the .iso file and run it in e.g. virtualbox, installing and playing around in it.

### Method 2: ready virtual machine

If you prefer to not have to install a system or it is a timesink installation (e.g. arch linux) - you can just download a ready virtualbox / vmware image from [osboxes](https://www.osboxes.org).

### Method 3: online - no installation

Thanks to the people over at [distrotest](https://distrotest.net) you are able to try out a lot of distros right from your browser, instead of having to install anything at all.

## I want to dualboot windows and linux

### Fix your clock booting off the same drive

If you decided that you can't have two drives to dual boot, at least make sure to fix your clock and possibly installing refind (on ubuntu: `sudo apt install refind` and confirming with enter when it asks you to) - refind is handy for windows not to break your boot selection screen and fixing your clock is necessary to avoid having linux or windows breaking the physical clock for each other.

Windows and Linux store their time in the BIOS differently, this will cause your clock to be desynchronized when you switch from one OS to the other.

The easiest solution for it is to fix it in Linux, forcing it to work the same way as Windows. You can do this through the terminal:

`timedatectl set-local-rtc 1 --adjust-system-clock`

You can verify if the change has been successful, by running this command:

`timedatectl`

You should see `RTC in local TZ: yes`. If you need to revert it, just set it to 0:

`timedatectl set-local-rtc 0 --adjust-system-clock`

### Method 1: on the same drive

It is possible, but you'll have a lot of issues down the road, because windows will during some updates ruin your boot or corrupt your linux partition, all things that happened before. If you still want to go ahead, then install windows first and then install ubuntu, that way ubuntu will auto detect windows and set itself all up. Otherwise if you already have linux as the first drive citizen - [install refind](https://www.rodsbooks.com/refind/installing.html#linux) on linux - on ubuntu it is as simple as doing `sudo apt install refind` and confirming to write to boot when it asks you to. Then you will need to shrink the linux partition, manually partition windows during installation, .. as you might guess - not an easy task for a newcomer - so ideally use any other method listed below, it not only is easier, but will also prevent all the funny windows issues described above.

### Method 2: on separate drives (recommended)

Ideally if you really need windows, install it to another drive entirely and [install refind](https://www.rodsbooks.com/refind/installing.html#linux) it will automatically detect windows and linux on separate drives for you, on ubuntu it's as simple as: `sudo apt install refind` and confirming when it asks you to install to boot.

### Method 3: use linux in a VM on windows (second best choice)

VMware allows you to use all your monitors in a VM, virtualbox is fine too, you don't need to dualboot and can enjoy windows for most of your activities (which assumingly are for gaming), it all depends which operating system you use most.

### Method 4: use windows in a VM on linux (advanced)

There's plenty documentation on virtualizing windows on linux, the easiest is to use [virt-manager](https://virt-manager.org/) - there's plenty tutorials on how to passthrough your GPU, so the VM can properly make use of it. If you plan to go this route, you will need two GPUs, one for the linux host and another for the windows VM if you plan to do anything intensive like gaming, otherwise you might be just fine using the built-in and pre-configured VNC/QXL video driver.

### Method 5: use windows in a VM on unraid (paid but easier than Method 4)

The same as Method 4, but using [unraid](https://unraid.net/), as mentioned - it is one-time paid software - but it is absolutely worth it in the long run as it gives you not only easy virtual machine managment, but also docker, apps, an easy configured [NAS](https://en.wikipedia.org/wiki/Network-attached_storage) and more. Linus Tech Tips did plenty of videos (e.g. [1](https://www.youtube.com/watch?v=LuJYMCbIbPk),[2](https://www.youtube.com/watch?v=opX-AsJ5Uy8)) demonstrating what kind of crazy setups you can have with unraid. Note that ideally you'd want a phone or laptop at hand to do some of the initial setup of unraid and later if you want to on the fly adjust things. You can however if you have a functioning passthrough setup just open up a browser and navigate the unraid web-ui freely.

## How do I create a persistent USB stick?

- If you're on windows, use [rufus](https://www.linuxuprising.com/2019/08/rufus-creating-persistent-storage-live.html) with supported distros.
- If you're on linux, there's [this tutorial](https://www.howtogeek.com/howto/14912/create-a-persistent-bootable-ubuntu-usb-flash-drive/).

If neither is good to you, you are searching for the keywords "persistent live usb" for google, plenty of other resources.

## I want to change / uninstall my DE (Desktop Environment) to another

This will leave you with a lot of pain, it's best to just get what's called a "flavor" (a distro with that DE pre-installed and ready with helpful / supporting packages) and migrate your data over. You can preview various flavors here: https://distrotest.net/ without a virtual machine or having to boot into a live-usb stick. 

As to answer the "why" you shouldn't uninstall - most DEs come with a lot of packages pre-installed and config files, which other DEs can't handle or need different versions of, leaving you with a broken installation. You can install them side by side and have your display manager (the screen you enter your password into when booting) decide which DE you want to boot into - as long as they are separate, it should be fine, however some manage to still clash and cause issues, so again - your best bet is to just re-install to a flavor that uses your new DE of choice and prior testing it either in a virtual machine or on distrotest.

## Can I play games on linux?

Yes, but no. You can check if your game is listed without major concerns / issues in [ProtonDB](https://www.protondb.com/) or [WineHQ](https://www.winehq.org/) but games are still windows territory, so your best bet is to use any of the [recommended methods](#method-2-on-separate-drives-recommended) to get windows for games setup.

Note: games that have anti-cheat, can outright ban you for using things like Steamplay/Proton/Wine/Lutris/.. so be careful and do it at your own risk, it's easier to just dualboot or run a windows VM, than risk a ban.

## Beginner or "just works"-experience linux distro?

Currently Kubuntu 20.04 seems to be the all around winner, except if you are looking for running it on desktop with a nvidia card. Then look for either Ubuntu 20.04 or Pop!_OS 20.04 - Pop offers dedicated images for nvidia, though testing regular Ubuntu it's also fine with nvidia cards.

## How to install and use wireguard?

Generally it's very easy and [documented on their installation page very well](https://www.wireguard.com/install/), just download wireguard, copy over the config, `wg-quick` it and optionally set it to start at boot. In step-by-step it would be:

1. `sudo add-apt-repository ppa:wireguard/wireguard && sudo apt update && sudo apt install wireguard` - that will add the repository for ubuntu based systems and install wireguard. (other systems see [wireguard pages](https://www.wireguard.com/install/))
2. copy your wireguard config to `/etc/wireguard/wg0.conf` (you can name it anything instead of wg0 too, just make sure to carry it over into other commands below)
3. `wg-quick up wg0` (that brings up the wireguard tunnel)
4. optionally to start at boot: `sudo systemctl enable wg-quick@wg0`

## How do I get asian layouts to work in Ubuntu / Linux Mint?

The instructions below assume Linux Mint, but the resources show the same path for regular Ubuntu.

Resources:
- https://leimao.github.io/blog/Ubuntu-Gaming-Chinese-Input/ 
    - [Mirror](https://web.archive.org/web/20200918234136/https://leimao.github.io/blog/Ubuntu-Gaming-Chinese-Input/)
- https://forums.linuxmint.com/viewtopic.php?t=264802
    - [Mirror](https://web.archive.org/web/20210412113326/https://forums.linuxmint.com/viewtopic.php?t=264802)

Install packages first:

```
sudo apt update
sudo apt install fcitx-bin
sudo apt install fcitx-googlepinyin
```

(it'll take a while for `fcitx-bin` to be installed)

open up `input method` make sure `fcitx` is selected at the top, install each language you want, in this example we'll install simplified chinese

![asian-keyboard-1](/images/asian-keyboard-1.png)

when installing a language it'll show a dialog like this each time to confirm

![asian-keyboard-2](/images/asian-keyboard-2.png)

after it's done installing all languages you want - log out

![asian-keyboard-3](/images/asian-keyboard-3.png)

now go to FCITX-config

![asian-keyboard-4](/images/asian-keyboard-4.png)

click `+` and untick `only show current language`

![asian-keyboard-5](/images/asian-keyboard-5.png)

once you select pinyin it'll work once you switch to it via ctrl+Enter (like the message said above in the input method)

![asian-keyboard-6](/images/asian-keyboard-6.png)

Done!
